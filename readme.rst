###################
About the Web App
###################

This is a web-based application that enables business owners to register the promotions and offers their businesses offer. The web app acts as the back end server for the main mobile app to be used by customers who are the end users.

The app will also incorporate an API for the mobile app.

*******************
Technology Stack
*******************
- PHP (Code Igniter Framework version 3.x)
- HTML 5
- CSS 3
- Javascript