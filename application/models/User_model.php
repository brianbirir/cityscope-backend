<?php
/**
*
*/
class User_model extends CI_Model
{
	public function __construct()
  {
  	// Call the CI_Model constructor
    parent::__construct();
  }

	// login
	function login($username, $password)
	{
		$this->db->where('email', $username);
	  $this->db->where('password', $password);
	  //$this -> db -> limit(1);

		// get table called 'user'
	  $query =$this->db->get("user");

		//check if row returned is only one
	  if($query->num_rows() == 1)
	  {
			foreach($query->result() as $rows)
		  {
		    //add all data to session
		    $newdata = array(
		      'uid'  => $rows->uid,
		      'username'  => $rows->username,
		      'email'    => $rows->email,
		      'logged_in'  => TRUE,
		    );
		  }
	 		$this->session->set_userdata($newdata);
	 		return true;
	   }
	   return false;
	}

	// login validation
	// to check if username and password are correct
	public function login_validation($email,$password)
	{

	}

	// add new user to database
	function add_user()
	{
		$data = array(
    	'username'=>$this->input->post('username'),
    	'email'=>$this->input->post('email'),
    	'password'=>md5($this->input->post('password'))
  	);
  	$this->db->insert('user',$data);
	}
}
?>
