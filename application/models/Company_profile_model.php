<?php
class Company_profile_model extends CI_Model
{
	public function __construct()
  {
  	// Call the CI_Model constructor
    parent::__construct();
  }

  // add new Company
  public function add_company() {
    $data=array(
      'company_name'=>$this->input->post('company-name'),
      'country'=>$this->input->post('country'),
      'city'=>$this->input->post('city'),
      'address'=>$this->input->post('address'),
      'zip'=>$this->input->post('zip'),
      'phone'=>$this->input->post('phone'),
      'email'=>$this->input->post('email'),
      'website'=>$this->input->post('website'),
      'category'=>$this->input->post('category'),
      'facebook'=>$this->input->post('facebook'),
      'twitter'=>$this->input->post('twitter'),
      'latitude'=>$this->input->post('coord-lat'),
      'longitude'=>$this->input->post('coord-lng')
  	);
  	$this->db->insert('company_profile',$data);
  }

}

 ?>
