<!DOCTYPE html>
<html>
    <head>
            <title>Citi Scope</title>
            <!--CSS Styles-->
            <?php
              //load helper
              $this->load->helper('html');

              // Bootstrap CSS
              echo link_tag('css/vendor/bootstrap.min.css');
              echo link_tag('css/custom/style.css');
            ?>
      <!-- initialize map-->
      <script type="text/javascript" src="<?php echo base_url();?>js/custom/google_map_api.js" ></script>
      <!--google map api-->
      <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9KSGMZxiOweHefq6xLSYg1tYlLCUATws&callback=initMap"></script>

    </head>
    <body>
      <!--navigation bar-->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
             <a class="navbar-brand cityscope-nav" href="user">City Scope</a>
          </div>
        </div>
      </nav>
          <div class="container">
