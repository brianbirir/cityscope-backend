<!DOCTYPE html>
<html>
    <head>
            <title>Citi Scope</title>
            <!--CSS Styles-->
            <?php
              //load helper
              $this->load->helper('html');

              // Bootstrap CSS
              echo link_tag('css/vendor/bootstrap.min.css');
              echo link_tag('css/custom/style.css');
            ?>
    </head>
    <body>
      <!--navigation bar-->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
             <a class="navbar-brand cityscope-nav" href="#">City Scope</a>
          </div>
        </div>
      </nav>
	     <h1><?php echo $title; ?></h1>
