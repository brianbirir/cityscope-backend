<div class="col-lg-9">
<?php echo validation_errors(); ?>
<div id="banner-form">
    <h1><?php echo $title; ?></h1>
			<!--connect to user login controller-->
		  <?php echo form_open('banner/post'); ?>
			  <div class="form-group">
			     <label for="upload-picture">Upload Banner Image:</label>
					 <?php echo form_error('email'); ?>
			     <input type="file" name="upload_image"/>
			  </div>

        <div class="form-group">
           <label for="promotion-title">Promotion Title:</label>
           <input type="text" size="20" id="promotion-title" name="promotion-title" class="form-control" required/>
        </div>

        <div class="form-group">
           <label for="last-name">Promotion Description:</label>
           <textarea id="promotion-desciption" name="promotion-description" class="form-control" required></textarea>
        </div>

        <div class="form-group">
           <label for="promotion-message">Promotion Marketing Message:</label>
           <textarea id="promotion-message" name="promotion-message" class="form-control" required></textarea>
        </div>

			  <input id="promotion-button" class="btn btn-primary" type="submit" value="Submit" name="promotion-button"/>
		   <?php echo form_close(); ?>
</div><!--close banner form-->
</div>
