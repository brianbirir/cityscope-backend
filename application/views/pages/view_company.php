<?php echo validation_errors(); ?>
<div id="registration-form">
	<h1><?php echo $title; ?></h1>
  <?php echo form_open('verifylogin'); ?>
	<div class="panel panel-primary locate-shop">
		<div class="panel-heading">Company Details</div>
			<div class="panel-body">
			  <div class="form-group">
			     <label for="username">Company Name:</label>
			     <input type="text" size="20" id="company-name" name="company-name" class="form-control" required/>
			  </div>
			  <div class="form-group">
			     <label for="password">Country:</label>
			     <input type="text" size="20" id="country" name="country" class="form-control" required/>
			  </div>
				<div class="form-group">
			     <label for="password">County:</label>
			     <input type="text" size="20" id="county" name="county" class="form-control" />
			  </div>
				<div class="form-group">
			     <label for="password">City:</label>
			     <input type="text" size="20" id="city" name="city" class="form-control" />
			  </div>
				<div class="form-group">
			     <label for="category">Categories</label>
			     <select class="form-control">
						 	<option>Restaurants</option>
			 		 		<option>Religion</option>
			 				<option>Real Estate</option>
					 </select>
			  </div>
				<div class="form-group">
			     <label for="password">Sub-Categories</label>
					 <select class="form-control">
						 	<option>Restaurants</option>
			 		 		<option>Religion</option>
			 				<option>Real Estate</option>
					 </select>
			  </div>
		</div>
	</div>

	<div class="panel panel-primary locate-shop">
		<div class="panel-heading">Company Contact Details</div>
			<div class="panel-body">
				<div class="form-group">
			     <label for="password">Postal Address</label>
			     <input type="number" size="20" id="postal-address" name="postal-address" class="form-control" />
			  </div>
				<div class="form-group">
			     <label for="password">Postal Code</label>
			     <input type="number" size="20" id="postal-code" name="postal-code" class="form-control" />
			  </div>
				<div class="form-group">
			     <label for="password">Phone</label>
			     <input type="number" size="20" id="phone" name="phone" class="form-control" />
			  </div>
				<div class="form-group">
			     <label for="password">Email</label>
			     <input type="email" size="20" id="email" name="email" class="form-control" />
			  </div>
				<div class="form-group">
			     <label for="password">Website</label>
			     <input type="url" size="20" id="website" name="website" class="form-control" />
			  </div>
				<div class="form-group">
					 <label for="facebook">Facebook</label>
					 <input type="text" size="20" id="facebook" name="website" class="form-control"/>
				</div>
				<div class="form-group">
					 <label for="twitter">Twitter</label>
					 <input type="text" size="20" id="website" name="website" class="form-control"/>
				</div>
		</div>
	</div>

	<div class="panel panel-primary locate-shop">
		<div class="panel-heading">Locate Shop</div>
		<div class="panel-body">
			<div class="form-group">
		     <label for="password">Latitude</label>
		     <input type="url" size="20" id="latitude" name="latitude" class="form-control"/>
		  </div>
			<div class="form-group">
		     <label for="password">Longitude</label>
		     <input type="url" size="20" id="longitude" name="longitude" class="form-control"/>
		  </div>
			<button class="btn btn-success">Locate shop</button>
		</div>
	</div>
     <input class="btn btn-primary" type="submit" value="Continue"/>
  </form>
</div>
