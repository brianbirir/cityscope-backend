

<div id="login-form">
	<?php
		if($login_error){
			echo '<div class="alert alert-danger"><strong>Warning: </strong>Wrong email or password</div>';
		}
	?>
	<div class="panel panel-primary locate-shop">
		<div class="panel-heading"><h1 class="login-form-heading"><?php echo $title; ?></h1></div>
			<div class="panel-body">

			<!--connect to user login controller-->
		  <?php echo form_open('user/login'); ?>
			  <div class="form-group">
						<!--use email to login -->
			     <label for="email">Email:</label>
					 <?php echo form_error('email'); ?>
			     <input type="email" size="20" id="email" name="email" class="form-control"/>
			  </div>
			  <div class="form-group">
			     <label for="password">Password:</label>
					 <?php echo form_error('password'); ?>
			     <input type="password" size="20" id="password" name="password" class="form-control"/>
			  </div>
			     <input id="login" class="btn btn-primary" type="submit" value="Login" name="login"/>
		   <?php echo form_close(); ?>
			 <!--close login form-->

			<!-- info to ask if user has not yet registered-->
			<div class="reg-info well">
				<p>Not registered? You can register an account <a href="register">here</a>.</p>
			</div>

		</div><!--end of panel body-->
	</div><!--end of panel-->
</div>
