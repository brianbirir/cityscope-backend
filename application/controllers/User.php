<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class User extends CI_Controller {

     function __construct()
     {
       parent::__construct();
       // load User_model
       $this->load->model('user_model');
       $this->load->model('user_profile_model');
       $this->load->helper('form');
       $this->load->helper('url');
       // load form validation helper
       $this->load->library('form_validation');
     }

     public function index($show_error= false)
     {
        $page='view_login';
        $data['title'] = ucfirst("login"); // Capitalize the first letter
        $data['login_error'] = $show_error;
        // check if user is already logged in
        // redirect to welcome page
        if(($this->session->userdata('user_name')!=""))
        {
         $this->welcome();
        } //otherwise redirect to the login page
        else {
          // check if login page exists
          if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
          {
            // Whoops, we don't have a page for that!
            show_404();
          }
          $this->load->view('templates/admin/admin_header'); // Pass 'login' title to the Page Title
          $this->load->view('pages/'.$page,$data);
          $this->load->view('templates/admin/admin_footer');
        }
     }

     // function for loading view displaying successful registration message
     public function successful_reg($page='view_successful_registration')
     {
       $data['title']= 'Successful Registration';
       $this->load->view('templates/admin/admin_header');
       $this->load->view('pages/'.$page, $data);
       $this->load->view('templates/admin/admin_footer');
     }

     // function for loading registration view
     public function registration_page($page='view_register')
     {
       if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
       {
            // Whoops, we don't have a page for that!
            show_404();
       }

       $data['title'] = ucfirst("register An Account"); // Capitalize the first letter

       $this->load->view('templates/admin/admin_header'); // Pass title to the Page Title
       $this->load->view('pages/'.$page,$data);
       $this->load->view('templates/admin/admin_footer');
     }

     // function for registering a new user
     public function register()
     {
       //error delimeter
       $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

       //field name, error message , validation rules
       $this->form_validation->set_rules('username','Username','trim|required|min_length[6]');
       $this->form_validation->set_rules('email','Email','trim|required|valid_email');
       $this->form_validation->set_rules('password','Password','trim|required|min_length[8]');
       $this->form_validation->set_rules('passconf','Confirm Password','trim|required|matches[password]');

       // if validation is FALSE redirect to index page
       if($this->form_validation->run() == FALSE)
       {
         // reload registration page
         $this->registration_page();
       } else {
         $this->user_model->add_user();
         redirect('successful-registration');
        }
     }

     // company details
     public function company($page='view_company')
     {
        $this->load->helper(array('form'));

         if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php')) {
                   // Whoops, we don't have a page for that!
                   show_404();
        }

        $data['title'] = ucfirst("Add Company Details"); // Capitalize the first letter

        $this->load->view('templates/admin/admin_header'); // Pass title to the Page Title
        $this->load->view('pages/'.$page,$data);
        $this->load->view('templates/admin/admin_footer');
     }

     // welcome view function after successful login
     public function welcome($page='view_welcome')
     {

       $data['title']= 'Welcome';
       $this->load->view('templates/admin/admin_header');
       $this->load->view('templates/admin/admin_side_bar');
       $this->load->view('pages/'.$page, $data);
       $this->load->view('templates/admin/admin_footer');
     }

     // login function
     public function login()
     {
       //load form validation lib
       $this->load->library('form_validation');
       //error delimeter
       $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
       //field name, error message , validation rules
       $this->form_validation->set_rules('email','Email','required');
       $this->form_validation->set_rules('password','Password','required');

       if($this->input->post('login')) {
         if($this->form_validation->run())
         {
           // get email and password from login form
           $email=$this->input->post('email');
           $password=md5($this->input->post('password'));

           // pass login form input values to user_model login function
           // and get query result
           $result=$this->user_model->login($email,$password);

           //check if result is true from database query
           if($result)
           {
             redirect('welcome');
           } else {
             $this->index(true);
           }
         } else {
           $this->index();
         }
      }

     }

     public function check_profile($uid)
     {
       // check if user is logging in the first time
       $check_for_profile_result = $this->user_profile_model->check_for_profile($uid);

       if(!$check_for_profile_result)
       {
         return True;
       }
     }

     // logout function
     public function logout()
     {
      $newdata = array(
        'uid'   =>'',
        'username'  =>'',
        'email'     => '',
        'logged_in' => FALSE,
      );
      // destroy session and redirect to index page
      $this->session->unset_userdata($newdata );
      $this->session->sess_destroy();
      //on logout redirect to home page
      redirect(base_url());
     }
   }
?>