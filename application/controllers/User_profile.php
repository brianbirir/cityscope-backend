<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class User_profile extends CI_Controller {

     function __construct()
     {
       parent::__construct();
       // load User_model
       $this->load->model('user_profile_model');
       $this->load->helper('form');
       $this->load->helper('url');
       // load form validation helper
       $this->load->library('form_validation');
     }

     // user profile view

     function profile($page = 'view_user_profile')
     {
       if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
       {
         // Whoops, we don't have a page for that!
         show_404();
       }

       //load user profile form
       $data['title'] = 'My Profile';
       
       $this->load->view('templates/admin/admin_header');
       $this->load->view('templates/admin/admin_side_bar');
       $this->load->view('pages/'.$page,$data);
       $this->load->view('templates/admin/admin_footer');
     }

     function complete_profile()
     {

     }

     // user profile registration function
     function user_profile_form()
     {
       //error delimeter
       $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

       //field name, error message , validation rules
       $this->form_validation->set_rules('first-name','First Name','trim|required');
       $this->form_validation->set_rules('last-name','Last Name','trim|required');
       $this->form_validation->set_rules('mobile','Mobile','trim|required');

       // if validation is FALSE reload the page
       if($this->form_validation->run() == FALSE)
       {
         $this->load->view('view_user_profile');
       } else {

          $this->user_profile_model->add_user_profile();
          $data['reg_message'] = "You have sucessfully registered your user profile!";
          // load company profile view and pass message that user
          // has successfuly registered
          load_company_profile_page($data);
        }
     }

     // function for company profile view that will be loaded after successful user profile registration
     function load_company_profile_page($data) {
       $page = 'view_company_profile';
       $this->load->view('templates/admin/admin_header');
       $this->load->view('templates/admin/admin_side_bar');
       $this->load->view('pages/'.$page,$data);
       $this->load->view('templates/admin/admin_footer');
     }
}
