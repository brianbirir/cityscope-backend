<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company_profile extends CI_Controller {

   function __construct()
   {
     parent::__construct();
     // load company profile model
     $this->load->model('company_profile_model');
     $this->load->helper('form');
     $this->load->helper('url');
     // load form validation helper
     $this->load->library('form_validation');
   }

   function view_company($page = 'view_company_profile')
   {
     if (!file_exists(APPPATH.'/views/pages/'.$page.'.php'))
     {
       // Whoops, we don't have a page for that!
       show_404();
     }

     // add session data
     //$user_id = $_SESSION['uid'];

     //load user profile form
     $data['title'] = 'Company Profile';
     //$data['uid'] = $user_id;

     $this->load->view('templates/admin/admin_header');
     $this->load->view('templates/admin/admin_side_bar');
     $this->load->view('pages/'.$page,$data);
     $this->load->view('templates/admin/admin_footer');
   }

   public function register()
   {

   }

}
?>
